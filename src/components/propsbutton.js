import React from 'react';
import {Image,ScrollView,StyleSheet, TouchableOpacity} from 'react-native';

const tombol = (props) => {
    return (
        <ScrollView horizontal>
            <TouchableOpacity onPress={props.onbuttonpress}>
                <Image source={require('../assets/like.png')} style={styles.edit}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source={require('../assets/komentar.png')} style={styles.edit}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source={require('../assets/bagikan.png')} style={styles.edit}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source={require('../assets/simpan.png')} style={{width: 25,height: 25,marginLeft:260,marginTop: 8,marginBottom: 15}}/>
            </TouchableOpacity>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    edit :{
        width: 27,
        height: 25,
        marginLeft: 15,
        marginTop: 8,
        marginBottom: 15
    }
});

export default tombol;